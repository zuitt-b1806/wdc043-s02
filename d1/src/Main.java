import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        //Operators in Java

        //Arithmetic: +, -, *, /, %
        //Comparison: >,<,>=, <=, ==, !=
        //Logical: && || !
        //Assignment: =

        //Control Structures in Java
            // If Statements: allows us to manipulate the flow of the code depending on the evaluation of logical expressions as our conditions
//        int num1 = 36;
//
//        if(num1 % 5 == 0){
//            System.out.println(num1 + " is divisible by 5.");
//        }
//        //else statement will allow us to run a task or code if the if condition fails or receives falsey value.
//        else{
//            System.out.println(num1 + " is not divisible by 5.");
//        }

        /*
        *   Mini Activity
        *   Create/Instantiate a new Scanner object from Scanner class and name it as numberScanner.
        *
        * Ask the user for an integer and store the input in a variable.
        * Add an if-selse statement if a number is even: show message: the number is even!
        * else, show the ff message:
        * number is odd!
        * */

        Scanner numberScanner = new Scanner(System.in);

//        System.out.println("Please input a number to know if it is an Even or Odd number");
//        int number = numberScanner.nextInt();
//
//        if(number % 2 == 0){
//            System.out.println(number + " is an even number");
//        }
//        else{
//            System.out.println(number + " is an odd number.");
//        }

        /*Switch Statement
            Are control flow structure that allows one code block to be run out of many other code blocks.
            We compare the given value against each cases, and if a case matches we will run that code block.
            This is mostly used if the user input or the input is predictable.
            */
//        System.out.println("Enter a number from 1-4 to see one of the four directions:");
//        int directionValue = numberScanner.nextInt();
//
//        switch (directionValue){
//            case 1:
//                System.out.println("North");
//                break;
//            case 2:
//                System.out.println("South");
//                break;
//            case 3:
//                System.out.println("East");
//                break;
//            case 4:
//                System.out.println("West");
//                break;
//            default:
//                System.out.println("Number inputted is out of range.");
//                break;
//        }

        //Arrays are objects that contain a fixed/limited number of values of a single data type.

        //Syntax
        //dataType[] identifier/name/variable = new dataType[numberOfElements];
        //dataType[] identifier/name/variable = {elementA, elementB,....};

        //Syntax A
        String[] newArray = new String[3];
        newArray[0] = "Clark";
        newArray[1] = "Bruce";
        newArray[2] = "Diana";
//        newArray[3] = "Barry"; error - the length of a Java array is set upon creation.
        //This will show the memory address of an array or the location of the array within the memory.
        System.out.println(newArray);
        //We use the arrays class to access methods to manipulate and access our array.
        System.out.println(Arrays.toString(newArray));
        //Sort Method

        Arrays.sort(newArray);
        System.out.println("Result of Array.sort()");
        System.out.println(Arrays.toString(newArray));

        Integer[] arrayNum = new Integer[3];
        arrayNum[0] = 54;
        arrayNum[1] = 12;
        arrayNum[2] = 67;
        System.out.println("Initial order of the arrayNum:");
        System.out.println(Arrays.toString(arrayNum));

        Arrays.sort(arrayNum);
        System.out.println("Order of items in arrayNum after sort():");
        System.out.println(Arrays.toString(arrayNum));

//        binarySearch() allows us to pass an argument/itetm            to search for within our array.
        String searchItem = "Bruce";
        int result = Arrays.binarySearch(newArray, searchItem);
        System.out.println("The index of " + searchItem + " is " + result);


        /*

        Array lists Are resizable arrays that function similarly to how arrays work in other languages work like JS.
       Using the new keyword inc resting an arayList does not require the datatype of the array list to be defined to avoid repetition.
        Syntax:
        ArrayList<dataType> identifier = new ArrayList<>();
         */
        ArrayList<String> students = new ArrayList<>();

        //ArrayList Methods
            //arrayListName.add(<itemToAdd>) - adds elements in our array list;
        students.add("Paul");
        students.add("John");
        System.out.println(students);
//        arrayListName.get(index) - retrieve items from our array list using its index.
        System.out.println(students.get(1));
//        arrayListName.set(index, value) - allows us to update an item or element by its index
        students.set(0, "George");
        System.out.println(students);
//        arrayLIstName.remove(index) - as the name suggeests, removes an element based on the passed index.
        students.remove(1);
        System.out.println(students);

        students.add("James");
        students.add("Wade");
        System.out.println(students);
        students.remove(1);
        System.out.println(students);

//        arrayListName.clear() -  clears out an entire elements in the array list.

        students.clear();
        System.out.println(students);

        //arrayListName.size() - gets the length of the array l;ist
        System.out.println(students.size());

        //arrays with initialized values:
        double[] doubleArr = { 76.54, 80.02, 85.54, 79.77};

        System.out.println(Arrays.toString(doubleArr));

        //        doubleArr[4] = 93.22;
//
//        System.out.println(Arrays.toString(doubleArr));

        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Junie", "Cong"));

        System.out.println(employees);

        employees.add("Red");
        System.out.println(employees);

        //Most objects in Java are defined as classes that contain a proper set of properties and methods. However, there might be cases where this is not appropriate for your use-case or you may simply want to store a collection of data that is in a key-value pair.
        //In Java, “keys” are also referred as “fields”.
        //Syntax:
//        HashMap<fieldDataType, valueDataType> identifier = new HashMap<>();
        HashMap<String, String> userRoles = new HashMap<>();

        //Add new fields in the hashmap;
        //hasMapName.put(<item>);
        userRoles.put("Anna", "Admin");
        userRoles.put("Alice", "User");

        System.out.println(userRoles);

        userRoles.put("Alice", "Teacher");
        System.out.println(userRoles);

        userRoles.put("Dennis", "Admin");

        System.out.println(userRoles);
        //retrieve values by fields;
        //hashMapName.get("field");

        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Dennis"));
        //The get method is case-sensitive.
        System.out.println(userRoles.get("dennis"));

        //remove a field-value
        //hashMapName.remove("field");
        userRoles.remove("Dennis");
        System.out.println(userRoles);

//        Retreiving hasmap keys/values
//        Syntax: hashMapName.keySet()
        System.out.println(userRoles.keySet());

    }
}