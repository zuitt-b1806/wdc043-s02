import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {

        Scanner fruitScanner = new Scanner(System.in);

        String[] fruits = new String[5];
        fruits[0] = "apple";
        fruits[1] = "avocado";
        fruits[2] = "banana";
        fruits[3] = "kiwi";
        fruits[4] = "orange";

        System.out.println("Fruits in stock: " + Arrays.toString(fruits));
        System.out.println("Which fruit would you like to get the index of: ");
        String fruit =  fruitScanner.nextLine().toLowerCase();

        int result = Arrays.binarySearch(fruits, fruit);

        if(result >= 0){
            System.out.println("The index of " + fruit + " is:" + result);
        }
        else{
            System.out.println("Fruit entered is not among the list. ");
        }



        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current Inventory consists of: ");
        System.out.println(inventory);
    }
}